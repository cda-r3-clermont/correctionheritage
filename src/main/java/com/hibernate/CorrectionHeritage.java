package com.hibernate;

import com.hibernate.relation.model.Camion;
import com.hibernate.relation.model.Marque;
import com.hibernate.relation.model.Moto;
import com.hibernate.relation.utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.GregorianCalendar;

public class CorrectionHeritage {
    public static void main(String[] args) {
        SessionFactory sf = new HibernateUtil().buildSessionFactory();
        Session session = sf.getCurrentSession();

        Transaction tx = session.beginTransaction();

        Marque marque1 = new Marque();
        marque1.setLibelle("Kawasaki");

        Marque marque2 = new Marque();
        marque2.setLibelle("Renault");

        Marque marque3 = new Marque();
        marque3.setLibelle("Tesla");

        session.save(marque1);
        session.save(marque2);
        session.save(marque3);

        Moto moto = new Moto();
        moto.setMarque(marque1);
        moto.setTwoPerson(true);
        moto.setHaveTopCase(false);
        moto.setImmatricultation("CP415AR");
        moto.setModele("Z9000");
        GregorianCalendar gc = new GregorianCalendar();
        moto.setMiseEnCirculation(gc);

        Camion camion = new Camion();
        camion.setPtac(2000);
        camion.setImmatricultation("COR4628");
        camion.setMarque(marque2);
        camion.setMiseEnCirculation(gc);

        session.save(camion);
        session.save(moto);

        tx.commit();

        session.close();
        sf.close();

    }
}
