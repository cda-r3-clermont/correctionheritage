package com.hibernate.relation.model;

import javax.persistence.*;

@Entity
public class Rat extends Animal {

    @Basic
    private boolean domesticAnimal;


    public boolean isDomesticAnimal() {
        return domesticAnimal;
    }

    public void setDomesticAnimal(boolean domesticAnimal) {
        this.domesticAnimal = domesticAnimal;
    }
}
