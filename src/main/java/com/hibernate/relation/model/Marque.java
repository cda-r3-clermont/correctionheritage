package com.hibernate.relation.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import com.hibernate.relation.model.Vehicule;

@Entity
@Table(name="marque")
public class Marque {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @OneToMany(mappedBy = "marque")
    private List<Vehicule> vehiculeList;

    @Basic
    private String libelle;

    public Marque() {
        this.vehiculeList = new ArrayList<Vehicule>();
    }

    public List<Vehicule> getVehiculeList(){
        return this.vehiculeList;
    }

    public void addVehicule(Vehicule vehicule){
        this.vehiculeList.add(vehicule);
    }

    public void removeVehicule(Vehicule vehicule){
        this.vehiculeList.remove(vehicule);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }
}
