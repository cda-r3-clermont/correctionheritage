package com.hibernate.relation.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("Moto")
public class Moto extends Vehicule{

    @Column(name = "is_two_person")
    private boolean isTwoPerson;


    @Column(name = "have_top_case")
    private boolean haveTopCase;


    public boolean isTwoPerson() {
        return isTwoPerson;
    }

    public void setTwoPerson(boolean twoPerson) {
        isTwoPerson = twoPerson;
    }

    public boolean isHaveTopCase() {
        return haveTopCase;
    }

    public void setHaveTopCase(boolean haveTopCase) {
        this.haveTopCase = haveTopCase;
    }
}
