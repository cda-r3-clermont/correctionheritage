package com.hibernate.relation.model;

import javax.persistence.*;
import java.util.Calendar;

@Entity
@Table(name = "vehicule")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@DiscriminatorColumn(name = "vehicule_type",
        discriminatorType = DiscriminatorType.STRING)
public abstract class Vehicule {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private long id;

    @ManyToOne
    private Marque marque;

    @Basic
    private String modele;

    @Column(length = 7)
    private String immatricultation;

    @Column(name = "mise_en_circulation")
    @Temporal(TemporalType.DATE)
    private Calendar miseEnCirculation;

    public Calendar getMiseEnCirculation() {
        return miseEnCirculation;
    }

    public void setMiseEnCirculation(Calendar miseEnCirculation) {
        this.miseEnCirculation = miseEnCirculation;
    }

    public String getImmatricultation() {
        return immatricultation;
    }

    public void setImmatricultation(String immatricultation) {
        this.immatricultation = immatricultation;
    }

    public Marque getMarque() {
        return marque;
    }

    public String getModele() {
        return modele;
    }

    public void setModele(String modele) {
        this.modele = modele;
    }

    public void setMarque(Marque marque) {
        this.marque = marque;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
