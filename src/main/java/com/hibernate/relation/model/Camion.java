package com.hibernate.relation.model;

import javax.persistence.Basic;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("Camion")
public class Camion extends Vehicule {
    @Basic
    private int ptac;

    public int getPtac() {
        return ptac;
    }

    public void setPtac(int ptac) {
        this.ptac = ptac;
    }
}
