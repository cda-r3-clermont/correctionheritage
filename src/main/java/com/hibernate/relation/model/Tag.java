package com.hibernate.relation.model;

import javax.persistence.*;
import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="tag")
public class Tag {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false)
    private String libelle;

    @ManyToMany(mappedBy = "tags")
    private List<Article> articles;

    public Tag() {
        this.articles = new ArrayList<Article>();
    }

    public void addArticle(Article article){
        this.articles.add(article);
    }

    public void removeArticle(Article article){
        this.articles.remove(article);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }
}
