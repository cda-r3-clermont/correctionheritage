package com.hibernate.relation.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("Voiture")
public class Voiture extends Vehicule{

    @Column(name = "have_clim")
    private boolean haveClim;

    @Column(name = "four_motion")
    private boolean fourMotion;

    public boolean isHaveClim() {
        return haveClim;
    }

    public void setHaveClim(boolean haveClim) {
        this.haveClim = haveClim;
    }

    public boolean isFourMotion() {
        return fourMotion;
    }

    public void setFourMotion(boolean fourMotion) {
        this.fourMotion = fourMotion;
    }
}
