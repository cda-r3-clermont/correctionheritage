package com.hibernate;

import com.hibernate.relation.model.Article;
import com.hibernate.relation.model.Chien;
import com.hibernate.relation.model.Rat;
import com.hibernate.relation.model.Tag;
import com.hibernate.relation.utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class Main {

        public static void main(String[] args) {
            SessionFactory sf = new HibernateUtil().buildSessionFactory();
            Session session = sf.getCurrentSession();

            Transaction tx = session.beginTransaction();

            Chien chien = new Chien();
            chien.setNom("Félix");
            chien.setHuntingDog(false);

            Rat rat = new Rat();
            rat.setNom("Ratus");
            rat.setDomesticAnimal(false);

            session.save(chien);
            session.save(rat);


            tx.commit();
            session.close();

            sf.close();

        }
}
